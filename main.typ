#import "conf.typ": conf
#import "colorbox.typ": colorbox


#let rainbow(content) = { // for rainbow text
  set text(fill: gradient.linear(..color.map.rainbow))
  box(content)
}


#set heading(numbering: "1.")
#show: doc => conf(
  title: [
    Principe de conteneurisation, motivations et mise en oeuvre de conteneurs avec l'implémentation Docker.
  ],

  authors: (
    (
      name: "BRESSON Benjamin",
      email: "bbresson@enssat.fr",
    ),
  ),
  doc,
)
#line(length: 100%)



#set heading(numbering: "1.")

#show outline.entry.where(
  level: 1
): it => {
  v(12pt, weak: true)
  strong(it)
}
#set highlight(fill: luma(200),bottom-edge: "descender", top-edge: "ascender", extent: 1pt) // all #highlight[] will follow these parameters

= Avant propos
#set text(size: 12pt)
#set par(justify:true)
Ce document a pour but d'expliquer les raisons qui ont mené à l'idée de conteneurisation, les possibilités qu'elle offre, ses avantages (et inconvénients) et de commencer à utiliser des implémentations telles que Docker, d'abord par des configurations simples puis en incrémentant des fonctionnalités. Il est destiné à des personnes ayant déjà des connaissances en informatique et sur l'univers Linux, mais pas forcément en conteneurisation. Il est donc possible que certaines notions ne soient pas expliquées en détail, mais des liens vers des ressources plus complètes sont donnés. Certaines autres notions sont expliquées en détail, car elles sont intéressantes en tant que telles. Ce document est aussi destiné à être lu en ligne, il contient donc des liens vers des ressources externes, n'hésitez pas à les consulter, notamment la documentation de Docker. Enfin, il est fort probable que ce document soit mis à jour, notamment sur la parties @docker_init[Initiation à docker] car Docker évolue constamment. Sur la version 1 du document, il n'y a pas de partie sur la création d'images de conteneur. Peut-être pour la version 2, ou 3 de ce document.
Pour suivre l'évolution des versions il y a, en annexe, un lien vers mon GitLab contenant les ressources, le code source ainsi que les PDF compilés dans la section #text("releases"), si j'ai bien pensé à faire tout ca. \

#align(bottom)[
#text("Nota bene", weight: "bold") : je ne suis pas un giga crack en ce qui concerne la conteneurisation et Docker ; ce document sert d'introduction à ce programme, ses possibilités et les exemples donnés sont à prendre tels quels et ne garantissent #text("ni un niveau de sécurité optimal ni un fonctionnement à toute épreuve.", weight: "bold") Il ne faut jamais perdre de vue que tout système informatique possède des failles de sécurité et que votre bon sens est votre meilleur allié. Si vous avez un doute, n'hésitez pas à vous orienter vers la documentation et les forums ou l'aide est de légion tant la conteneurisation est vastement utilisée aujourd'hui. Enfin, nous verrons ici une mise en place limitée de Docker en termes de taille, il existe des solutions plus complètes telles que Kubernetes ou Swarm pour gérer des infrastructures de conteneurs, mais nous n'aborderons pas ces sujets ici.
]

#pagebreak() 

= Un peu d'histoire
== C'est quoi un conteneur dans la vraie vie ?

Il peut sembler saugrenu de parler des conteneurs "de la vraie vie" pour introduire la conteneurisation, mais comprendre ce qu'est un tel objet permet de mieux appréhender le principe informatique ainsi que les motivations qui ont poussé à la création de ceux-ci.

Un conteneur #text("contient", style: "italic"). Cela permet le transport de marchandises de manière #text("sécurisée et efficace", style: "italic") et par des moyens différents (avion, bateau, camion, train ...). Cela est rendu possible par le fait qu'ils sont #text("standardisés", style: "italic") et que les moyens de transport sont adaptés à leur gabarit. Ainsi, déplacer un conteneur entre deux moyens de transports différents est facile et rapide. Le premier standard pour les conteneurs a été établi en 1933 par le Bureau international des containers et du transport intermodal #footnote[#link("fr.wikipedia.org/wiki/Conteneur")]. L'idée de conteneur n'est donc pas toute jeune.

== Début de la conteneurisation en informatique
Une première implémentation d'isolation d'un processus est apparue avec #text("chroot", font: "DejaVu Sans Mono", size: 10pt), qui modifie le répertoire racine ou #text("root", style: "italic") apparent du processus en cours et de ses fils (#text("chroot", font: "DejaVu Sans Mono", size: 10pt) \<\-\-\> change root). On parle alors de prison #text("chroot", font: "DejaVu Sans Mono", size: 10pt) ou #text("chroot", font: "DejaVu Sans Mono", size: 10pt) jail. Toutefois, #text("chroot", font: "DejaVu Sans Mono", size: 10pt) présente des limitations et n'a pas pour but de protéger d'attaques telles de que l'élévation de droits. En effet, sur la plupart des systèmes, les contextes de chroot ne s'empilent pas correctement et les programmes chrootés disposant de privilèges suffisants peuvent effectuer un second chroot pour s'en échapper#footnote[#link("en.wikipedia.org/wiki/Chroot")]. C'est un programme utile dans de nombreux cas, la page Wikipedia de #text("chroot", font: "DejaVu Sans Mono", size: 10pt) donne l'exemple de ChromeOS qui utilise #text("chroot", font: "DejaVu Sans Mono", size: 10pt) pour exécuter des applications Linux au lieu de recourir à de la virtualisation. J'ai, à titre personnel, déjà fait affaire à #text("chroot", font: "DejaVu Sans Mono", size: 10pt) afin de réparer un GRUB cassé suite à une mise à jour Windows sur mon PC en dual-boot.

Il y a une tendance qui se dégage : on veut faire tourner des programmes "dans leur coin" et configurer finement la manière dont ils peuvent interagir avec le système hôte.

Toutefois, la solution du #text("chroot", font: "DejaVu Sans Mono", size: 10pt) n'est pas satisfaisante et qu'il manque encore des solutions complètes, il faut aller plus loin dans le principe.



= Principe de conteneurisation
La conteneurisation est une méthode de déploiement d'applications qui permet plusieurs choses : d'une part d'empaqueter une application et ses dépendances (ce qui est fort pratique) et d'une autre part de l'isoler du système d'exploitation hôte. Cela permet de s'affranchir des problèmes de compatibilités entre les dépendances ; un programme un peu vieux peu déprendre de bibliothèques qui ne sont plus disponibles sur le système que sous des versions trop récentes (i.e. incompatibles).
Ainsi, c'est là que l'API _containerd_ fait son entrée. Containerd gère tout le cycle de vie d'un conteneur, de son instanciation à sa suppression. Il est voulu léger, rapide et flexible pour le rendre adaptable à pleins d'environment. Il suit les spécifications de l'Open Container Initiative#footnote("un projet de la Fondation Linux : opencontainers.org"), ce qui permet de rendre compatibles d'autres outils et plateformes qui suivent ce standard. 
== La conteneurisation n'est pas la virtualisation
À ce stade, il est tout à fait possible de confondre conteneurisation et virtualisation, mais ce sont deux concepts différents. La virtualisation utilise un hyperviseur pour faire tourner des systèmes d'exploitation #text("en entier", weight: "bold") sur un même hôte. Tandis que la conteneurisation ne consiste qu'en le paquet d'uniquement ce qui est #text("nécessaire", style: "italic") pour faire tourner le programme en question, *on n'a pas besoin de faire tourner un autre noyau* comme en virtualisation. La figure suivant démontre bien les différences entre conteneurisation et virtualisation.
#figure(
  image("images/diff-cont-virt.png"),
  caption: [Les différentes couches nécéssaires pour la virtualisation à gauche et la conteneurisation à droite.],
)
On peut s'affranchir des systèmes d'exploitation nécessaires au fonctionnement des machines virtuelles, ce qui rend les conteneurs #text("beaucoup", style: "italic") plus légers que des VM que ce soit en termes de mémoire ou de ressources CPU/GPU. Enfin, les conteneurs sont _scalables_#footnote("Anglicisme, peut se traduire par échelonable, extensible, évolutif"), ainsi, un service qui n'est que peu sollicité, ne va consommer que peu de ressources et ainsi laisser le reste aux autres services. Mais si un service est fortement sollicité, il peut consommer plus de ressources et on peut y fixer des limites.

== Avantages de la conteneurisation
Ils ont été évoqués dans les parties précédentes, mais voici une liste non exhaustive des avantages de la conteneurisation :
#list(
  [Portabilité grâce à la standardisation],
  [Légèreté ; pas besoin d'un noyau pour chaque conteneur],
  [Facilité de mise en place et de déploiement d'un conteneur, ce qui permet la mise à jour d'une application presque sans interruption de service],
  [Les technologies implémentant la conteneurisation sont open source et présentes sur à peu près toutes les plateformes],
  [L'isolation des conteneurs entre eux et avec le système hôte permet d'améliorer la sécurité ainsi que la stabilité du système],
  [Scalabilité ; un conteneur ne consomme que ce dont il a besoin, pas besoin de lui allouer des ressources fixes],
  [Permet de bien séparer les responsabilités entre les développeurs et les administrateurs système (merci GitHub copilot pour cette phrase)],
  [Si deux applications ont besoin du même port san configuration simple possible, il est possible de les faire tourner sur le même hôte en utilisant des conteneurs],
  [Permet un contrôle des ressources allouées à chaque conteneur que ce soit en termes d'espace mémoire, de ressources CPU ou de bande passante réseau],
  [Lorsque l'infrastructure des conteneurs est grande, il existe des solution telles que Kubernetes (K8s) qui permettent de gérer l'orchestration des conteneurs],
  [C'est une technologie très simple et rapide à appréhender et à mettre en place],
  [Ça fait déjà beaucoup de points positifs là non ?]
)

== Inconvénients de la conteneurisation
C'est chaud d'en trouver là. \
On verra plus tard pourquoi, mais un conteneur à une mémoire volatile par défaut ; si on en déploie un, puis qu'on fait des configurations et qu'on le supprime, on perd tout, il faut donc bien penser à utiliser des volumes pour stocker les données. De plus, certains programmes malveillants ont réussi à s'échapper de conteneurs, il faut donc faire bien attention à quel conteneur on déploie sur son système. Nous verrons plus loin des sources fiables d'images de conteneurs (nous verrons aussi ce qu'est une image de conteneur).


= Initiation à Docker <docker_init>
== Installer Docker Engine
Maintenant, c'est la partie #text("vraiment", weight: "black") fun. On va enfin déployer un conteneur avec Docker. Enfin presque, il faut d'abord installer le moteur docker ou #text("docker engine", style: "italic"). Pour cela, tout dépend du système d'exploitation que vous utilisez : sur windows, Docker s'installe grâce à WSL (Windows Subsystem for Linux), mais on ne va traiter que l'installation sur Linux ici. Je vous invite à ouvrir le lien suivant : #link("https://docs.docker.com/engine/install/") et suivre les instructions pour votre distribution Linux (attention, il faut aller dans la section #text("server", style: "italic") et ne pas installer Docker desktop, on n'en aura pas besoin). Une fois l'installation terminée, on peut lancer la commande ```bash sudo docker -v``` qui nous retourne la version de Docker Engine qui tourne actuellement sur la machine.

== Les commandes de base

#colorbox(
  title: "La commande si on est perdu",
  color: "blue",
  radius: 2pt,
  width: auto,
)[
  #align(center)[```bash sudo docker ps ```]
]
\*le flag ```bash -a ```permet de lister les conteneurs stoppés.


Cette commande contient deux mots clés : #text("sudo", font: "DejaVu Sans Mono", size: 10pt, fill: red) pour exécuter la suite en mode super utilisateur (root), #text("docker", font: "DejaVu Sans Mono", size: 10pt, fill: blue) pour exécuter le binaire du même nom et ```typc ps``` pour lister les conteneurs et quelques informations telles que leur nom, leur ID etc...
Si vous lancez cette commande maintenant, avant d'avoir créé un conteneur, voici ce que nous devriez obtenir dans le terminal :
#image("images/dockerpsvide.png", height: 40pt)
Il n'y a aucun conteneur qui tourne actuellement sur la machine. On va donc en créer un. Pour cela, on va d'abord lancer la commande suivante : 
#align(center)[
  #block(
  fill: luma(230),
  inset: 8pt,
  radius: 4pt,
  [ ```bash sudo docker -it alpine``` ],
  )
]

#colorbox(
  title: "Créer un conteneur Alpine",
  color: "green",
  radius: 2pt,
  width: auto,
)[
  #align(center)[```bash sudo docker -it alpine ```]
]

Cette commande va télécharger l'image #text("alpine", style: "italic") (c'est une distribution Linux très légère #footnote[#link("alpinelinux.org/")]) car elle n'est encore présente sur le système. Pour faire simple, une image est comme un template pour un conteneur. L'option  ```bash -it``` peut se décomposer en ```bash -i``` et ```bash -t```. Le premier flag indique qu'on lance le conteneur en mode interactif : cela permet de connecter les entrées et sorties standard du conteneur et du terminal entre elles et ainsi d'interagir avec le conteneur par des commandes. Le second flag permet de simuler un TTY (Teletype) et d'avoir l'interface d'un shell. On peut avoir plusieurs conteneurs à partir d'une même image. Une fois qu'on a lancé la commande précédente, on se retrouve face à un shell (en tant qu'utilisateur root, on peut le voir en lançant la commande ```bash whoami```). Si on fait un ```bash ls``` on se situe à la racine du conteneur. Explorez un peu les fichiers et surtout le contenu du ```bash/home``` : il est vide !
Maintenant, on peut faire un peu ce qu'on veut ! Nous allons nous détacher du conteneur (de son shell, pour être plus précis). Pour cela, il faut, tout en maintenant Ctrl appuyé, presser p puis q. Le terminal affiche ```typc read escape sequence```. On se retrouve alors sur notre shell hôte. Si on lance la commande ```bash sudo docker ps``` voici la sortie sur le terminal :
#image("images/dockerps.png", height: 30pt)
On y voit le container ID, le nom de l'image utilisée pour ce conteneur, la commande exécutée au démarrage de celui-ci, depuis quand il a été créé, son status et depuis quand il y est, les éventuels ports qu'il utilise et son nom (on peut référencer un conteneur soit par son nom, soit par son ID). Rattachons-nous à notre conteneur.

#colorbox(
  title: "Se rattacher à un conteneur",
  color: "orange",
  radius: 2pt,
  width: auto,
)[
  #align(center)[```bash sudo docker attach busy_nightingale```]
]


On se retrouve alors de nouveau sur le shell du conteneur.


=== Notion de volume (Bind mounts)
Maintenant, essayons de créer un fichier par la commande ```bash touch``` en lui donnant un nom. Ensuite, on fait un ```bash exit``` : le conteneur se termine. Faisons, un ```hash sudo docker ps``` : il n'y a plus de conteneur en cours de fonctionnement. Si on veut voir tous les conteneurs, on peut faire  ```bash sudo docker ps -a```, voici la sortie :
#image("images/dockerpsa.png", height: 25pt)
Rien n'a changé par rapport à la sortie d'avant, sauf l'état du conteneur : exited accompagné du code de sortie qui indique la raison du exit (130 = 128 + 2  = Ctrl+C  $equiv$ exit#footnote[#link("tldp.org/LDP/abs/html/exitcodes.html") : les codes de sortie de Docker suivent la norme utilisée par chroot.]).
Si on relance la commande ```bash sudo docker run -it alpine``` et qu'on faut un ```bash ls``` : terrible, il n'y a plus le fichier qu'on a créé juste avant ! Mais pas de panique. Quittons ce nouveau conteneur. Si on lance ```bash sudo docker ps -a``` on peut voir nos deux conteneurs alpine qu'on a créé. En lançant ```bash sudo docker start -a -i containerID``` avec le containerID du conteneur où l'on a créé le fichier, on se retrouve de nouveau sur le shell de notre conteneur. Attention toutefois, si on supprime ce conteneur, sa zone mémoire va aussi être supprimée (donc libérée en soit) et donc perdue ! Voici la commande pour supprimer un conteneur : ```bash sudo docker rm containerID``` avec le containerID du conteneur à supprimer. Ainsi, on ne peut plus exécuter la commande précédente : ```bash sudo docker start -a -i containerID``` (avec toujours le container ID du conteneur auquel on s'intéresse) : le retour de cette commande est : ```typc Error response from daemon: No such container: containerID```, en effet le conteneur qu'on vient de supprimer n'existe plus. On peut vérifier cela en lançant ```bash sudo docker ps -a``` : il n'y a plus le conteneur qu'on vient de supprimer (logique).
Le problème de ce qu'on vient de faire, c'est qu'on a perdu notre fichier précédemment créé dans le conteneur, or il se pourrait qu'on en ai besoin. Nous allons donc voir comment associer un répertoire sur l'hôte avec celui d'un conteneur à sa création. Pour cela, il faut d'abord créer un répertoire que nos futurs conteneurs vont pouvoir utiliser. On lance la commande ```bash mkdir dossier_conteneur``` dans le répertoire #highlight[```typc /home/utilisateur/```]. Ensuite, on va utiliser une commande très similaire à celle d'avant ; #highlight[```bash sudo docker run -it -v "/home/utilisateur/dossier_conteneur:/dossier_conteneur" alpine```]. On a rajouté #highlight[```bash -v "/home/utilisateur/dossier_conteneur:/dossier_conteneur"```]. Le flag #highlight[```typc -v```] signifie que l'argument suivant est un #text("volume", style: "italic"), celui ci se découpe en deux parties, la partie à gauche du ":" représente le volume (un répertoire ici) dans le stockage de l'hôte et la partie à droite du ":" correspond au volume (un répertoire ici aussi) dans le stockage virtuel du conteneur (qui est réel sur le disque en fait). Si on fait un #highlight[```typc ls```] on va voir que le dossier qu'on a créé précédemment est présent dans le conteneur. Tout ce qu'on va mettre dans ce répertoire, que ce soit depuis le conteneur ou depuis l'hôte, va être visible des deux côtés.

=== Notion de port mapping
Supposons maintenant qu'on dispose de deux serveurs web, chacun utilisant le port 443 (port https par défaut) et que, pour x ou y raisons, on ne puisse pas changer le port qu'ils écoutent. Avec un conteneur, on peut mettre en place le port mapping. Par exemple, on dispose de deux serveurs webs tournant chacun sur un conteneur Alpine. On veut qu'un premier serveur écoute le port 445 et que l'autre écoute 446 (en supposant qu'il sont disponibles). Pour cela, on va lancer les conteneurs avec les commandes suivantes :

#align(center)[
  #block(
  fill: luma(230),
  inset: 8pt,
  radius: 4pt,
  [ ```bash sudo docker run -it -p 445:443 alpine``` ],
  )
]



#align(center)[et]
#align(center)[
  #block(
  fill: luma(230),
  inset: 8pt,
  radius: 4pt,
  [ ```bash sudo docker run -it -p 446:443 alpine``` ],
  )
]
Ces deux commandes vont associer le port 445 de l'hôte au port 443 du premier conteneur, et le port 446 de l'hôte au port 443 du second conteneur. Voilà, on a résolu un problème de conflit d'utilisation de ports aussi vite que \
On peut évidemment ajouter le flag ```typc -v "volume_disque:volume_conteneur"``` si besoin.

=== Quelques autres flags utiles
Voici une liste des flags les plus couramment utilisés avec Docker :
#list(
  [```bash --name nom_du_conteneur``` pour donner un joli nom à votre conteneur, les commandes stop, run; attach etc ... fonctionneront avec le nom donné au conteneur tout comme avec le containerID],
)
#pagebreak()
=== Quelques commandes utiles


#colorbox(
  title: "Voir les logs d'un conteneur",
  color: "blue",
  radius: 2pt,
  width: auto,
)[
  #align(center)[```bash sudo docker logs -f nom_ou_id_du_conteneur```]
]


Permet d'afficher les logs du conteneur (s'il y en a) en mode #text("follow", style: "italic"), donc dynamiquement.

== Mais il y a mieux
Les commandes pour lancer des conteneurs, surtout si on ajoute plusieurs volumes, des variables, des port mapping etc ... peuvent devenir très lourdes. C'est pourquoi nous allons désormais utiliser Docker Compose qui est un outil qui utilise un fichier YAML pour définir tout ce qu'une commande Docker run classique fait mais en plus clair et permet d'utiliser les même commandes pour tous les différents conteneurs qu'on veut lancer.


= Docker Compose
On a vu comment exécuter des conteneurs, mais il existe l'utilitaire compose (c'est un plugin de Docker) qui permet de lancer des conteneurs avec des fichiers de description qui remplacent les commandes ```typc docker run```. Cet outil permet aussi de lancer plusieurs conteneurs à l'aide du même fichier pour simplifier le déploiement d'une infrastructure en entier. Ces fichiers de description s'écrivent en YAML. Attention, les tabulations sont importantes, sinon une erreur sera retournée. \
Voici un exemple de fichier de description pour un conteneur :
```yaml
version: '3.3'
services:
  db:
    image: mysql:8.0
    restart: always
    environment:
      MYSQL_ROOT_PASSWORD: 'a_super_password'
    ports:
      - '3306:3306'
    volumes:
      - /conteneurs/mysql/var/lib/mysql:/var/lib/mysql
```
Ce fichier comporte plusieurs sections qui permettent de faire comme les commandes docker run, mais cela permet de ne pas avoir à retenir tous les attributs qu'on veut utiliser. En lisant ce fichier, on a par ordre des lignes :
#list(
  [la version de Docker Compose à utiliser (ici 3.3)],
  [la section services contient les conteneurs à exécuter, ici on ne lance qu'un seul conteneur qu'on nomme ```typc db``` puis les autres attributs sont :
  #list([l'image à choisir pour le conteneur],
    [la politique de redémarrage (on détaillera après ce que cela signifie)],
    [les variables d'environnement à passer au conteneur],
    [les ports à mapper],
    [les volumes à utiliser],
    )
  ],
)
On crée un fichier nommé ```typc docker-compose.yml``` puis on exécute la commande ```bash sudo docker compose up -d``` dans le ême répertoire que le docker-compose.yml (ou bien avat le up -d, on ajoute -f chemin_du_yml), cela va créer puis exécuter le conteneur en mode détaché (#text("daemonized", style: "italic")). On peut toujours utiliser les commandes docker pour lister les conteneurs.
Si on veut ensuite stopper le conteneur, il faut lancer la commande ```bash sudo docker compose stop```. Si on veut supprimer le conteneur (il doit d'abord être arrêté) il faut lancer ```bash sudo docker compose rm```. Si on veut directement supprimer un conteneur qui est actif, la commande  ```bash sudo docker compose down``` permet de le faire.

= Mise en pratique avancée

== Lancer son propre cloud
Voici un exemple de fichier YAML :
```yaml
version: '3.8'
services:
  nextcloud:
    image: lscr.io/linuxserver/nextcloud:latest
    container_name: nextcloud
    networks:
      cloud_stack:
        ipv4_address: 10.5.0.5
    environment:
      - PUID=1000
      - PGID=100
      - TZ=Europe/Paris
    volumes:
      - /home/benjamin/docker/cloud_stack/nextcloud/appdata:/config
      - /home/benjamin/docker/cloud_stack/nextcloud/data/nextcloud_data:/data	
    depends_on:
      - mysql-db
    ports:
      - 4444:443
      - 8084:80
    restart: unless-stopped
  
  mysql-db:
    container_name: mysql-db
    image: mysql:8.0
    networks:
      cloud_stack:
        ipv4_address: 10.5.0.6
    restart: always
    environment:
      MYSQL_ROOT_PASSWORD: 'ASuperPassword'
    volumes:
      - /home/benjamin/docker/cloud_stack/mysql-db/var/lib/mysql:/var/lib/mysql

networks:
  cloud_stack:
    driver: bridge
    ipam:
     config:
       - subnet: 10.5.0.0/16
         gateway: 10.5.0.1
```

À première vue, ce fichier à l'air barbare, mais il est en réalité assez simple. Comme pour la base données, on dispose de la version de Docker Compose à utiliser et des services à lancer. Ici, il y en a deux, avec un réseau (dénoté par network). Le premier service, intitulé nextcloud, se base sur une image construire par le collectif #text(style: "italic")[#link("linuxserver.io")] qui met à disposition des images de conteneurs pour des services assez répandus. N'hésitez pas à vous rendre sur leur dépôt, il contient de nombreux services conteneurisés avec une documentation assez large. \
Revenons-en à nos moutons. En lisant ligne par ligne, on donne un nom à ce conteneur, on l'associe à un réseau (celui tout en bas du fichier) et on lui donner une ipv4 (fixe), on fixe des variables d'environnement qui sont le user ID, le group ID et la timezone, ensuite on crée deux volumes ; un pour le dossier de configuration de Nextcloud et l'autre qui correspond à l'emplacement où Nextcloud va mettre les fichiers, on précise aussi que ce conteneur dépend d'un autre : mysql-db, puis on associe les port 4444 et 8084 de l'hôte aux ports 443 et 80 du conteneurs respectivement.\
Le second service est une base de données MySQL, le mot de passe de l'utilisateur root de cette base de données est fixé à "ASuperPassword", #text(weight: "extrabold")[IL FAUT IMPÉRATIVEMENT LE CHANGER, C'EST UN POINT CRITIQUE DE SÉCURITÉ]. Le volume qu'on crée est l'emplacement de la base de données en elle-même. Enfin, on défini un réseau, "cloud_stack" qui utilise le pilote bridge, puis on définit le groupe et sa taille et son default gateway.
Une fois ce fichier lancé avec la commande ```bash sudo docker compose up -d```, il faut se connecter sur le conteneur de la base de données et créer un utilisateur et une base de données puis lui associer les droits sur celle-ci. Une fois cela fait, sur l'interface de Nextcloud (ip_de_votre_machine:4444) il faut renseigner le nom de l'utilisateur tout juste créé et son mot de passe ainsi que la base de données, puis il faut préciser l'adresse  de celle-ci (10.5.0.6) et le port (3306 par défaut). 


== Gérer graphiquement des conteneurs 
\/\/\/ PORTAINER \/\/\/


= Créer des images Docker 
Dockerfile


= Annexes




= Bibliographie
https://www.redhat.com/fr/topics/cloud-native-apps/what-is-containerization
https://fr.wikipedia.org/wiki/Conteneur
https://fr.wikipedia.org/wiki/Open_Container_Initiative
https://www.opentext.com/file_source/OpenText/en_US/PDF/opentext-wp-docker-and-kubernetes.pdf
https://docs.docker.com/

= Remerciements
Soso le dozo, Gaby, Elyakim \
Ce document a été écrit à l'aide de Typst, une alternative open source à LaTex.