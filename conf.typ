#let conf(
  title: none,
  authors: (),
  matiere: none,
  doc,
) = {
  // Set and show rules from before.


  // Set the logo and matiere
  // grid(
  //   columns: (1fr, 1fr),
  //   align(center, image("containers.jpg", height: 500pt)), 
  //   align(right, text(12pt, matiere)),
  // )

  line(length: 100%)
  v(25pt)
  set align(center)
  align(text(23pt,  title), horizon)
  v(25pt)
  line(length: 100%)
  v(50pt)
  image("images/docker-quick-setup-logo.png", height: 250pt)
  v(150pt)
  let count = authors.len()
  let ncols = calc.min(count, 3)

  grid(
    columns: (1fr,) * ncols,
    row-gutter: 24pt,
    ..authors.map(author => [
      #author.name \
      // #author.affiliation \
      #link("mailto:" + author.email)
    ]),
  )


  //pagebreak()
  set page(
  numbering: "1 / 1",
  )
  outline(
    title: "Table des matières",
    indent: auto,
  )

  pagebreak()

  set align(left)
  doc
}