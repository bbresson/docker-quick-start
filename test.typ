
#set heading(numbering: "1.")

#show outline.entry.where(
  level: 1
): it => {
  v(12pt, weak: true)
  strong(it)
}

#outline(title : "bonjour",indent: auto)
= bonjour
== test1
=== test2
==== test3 
#lorem(10)
#grid(
    columns : (1fr,1fr),
    [VBVBV],
    [VBVBV]

     )

$forall lambda in FF "&" "bonjour" "OIUYGTHJKIUFYHJK" "cac" "forall" "bonjour" ""$

Adding `rbx` to `rcx` gives
the desired result.

What is ```rust fn main()``` in Rust
would be ```c int main()``` in C.

```rust
fn main() {
    println!("Hello World!");
}
```

```c
int main(int argc, char * argv[]) {
  printf("Hello World!");
}
```

This has ``` `backticks` ``` in it
(but the spaces are trimmed). And
``` here``` the leading space is
also trimmed.
bonjour 